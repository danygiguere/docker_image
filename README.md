For local installation, please run sh local-install.sh

docker build . -t dockerenvwebsite
locally :
docker run --rm -it -p 8000:80 -v $PWD:/var/www dockerenvwebsite
production:
docker run --rm -it -p 8000:80 dockerenvwebsite

go to:
http://127.0.0.1:8000/
